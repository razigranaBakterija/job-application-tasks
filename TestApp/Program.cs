﻿using System;
using System.IO;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Text;

namespace TestApp
{
    class MainClass
    {
        static void Main(string[] args)
        {
            var tasks = new Tasks();

            // First task
            Console.WriteLine("\n\nFirst task:");

            String testString = "awrgbsgtd fjjg";
            var abc = tasks.MostOccurences(testString);
            if (abc.Equals('\0'))
            {
                Console.WriteLine("Error occured");
            }
            else
            {
                Console.WriteLine("In string : " + testString + ", char that occured the most times is : " + abc);
            }

            // Second task
            Console.WriteLine("\n\nSecond task:");

            String a = "asdf";
            String b = "fdas";
            if (tasks.isAnagram(a, b))
            {
                Console.WriteLine("String : " + a + " and string : " + b + " are anagrams.");
            }
            else
            {
                Console.WriteLine("String : " + a + " and string : " + b + " are not anagrams.");
            }

            // Third task
            Console.WriteLine("\n\nThird task:");

            var dir = "d:\\Games";
            var fileName = tasks.findLongestFileNameInDir(dir);
            Console.WriteLine("longest file name in dir : " + dir + " is : " + fileName);

            // Fourth task
            Console.WriteLine("\n\nFourth task:");

            var avg = tasks.ProsjecanTecajHrkUsd();
            Console.WriteLine("average USD to HRK : " + avg);

            // Fifth task
            Console.WriteLine("\n\nFifth task:");

            String[] permutations = tasks.Permutations("abcd");
            foreach (String str in permutations)
            {
                Console.WriteLine("permutation : " + str);
            }
        }
    }

    class Tasks
    {
        public char MostOccurences(String input)
        {
            var chars = input.ToCharArray();
            var map = countOccurences(chars);

            var maxOccurences = 0;
            char charToReturn = '\0';

            foreach (KeyValuePair<char, int> entry in map)
            {
                var value = entry.Value;

                if (value > maxOccurences)
                {
                    maxOccurences = value;
                    charToReturn = entry.Key;
                }
            }

            return charToReturn;
        }

        public Boolean isAnagram(String a, String b)
        {
            if (a.Length != b.Length)
            {
                return false;
            }
            
            var mapA = countOccurences(a.ToCharArray());
            var mapB = countOccurences(b.ToCharArray());
            foreach (KeyValuePair<char, int> entry in mapA)
            {
                if (mapB.TryGetValue(entry.Key, out int value))
                {
                    if (entry.Value == value)
                    {
                        continue;
                    }
                }

                return false;
            }

            return true;
        }

        private ConcurrentDictionary<char, int> countOccurences(char[] chars)
        {
            var map = new ConcurrentDictionary<char, int>();
            for (int i = 0; i < chars.Length; i++)
            {
                var key = chars[i];
                map.AddOrUpdate(key, 1, (key, value) => value + 1);
            }

            return map;
        }

        public String findLongestFileNameInDir(String directoryPath)
        {
            var allFiles = Directory.GetFiles(directoryPath, "*", SearchOption.AllDirectories);
            int longestNameIndex = 0;
            for (int i = 1; i < allFiles.Length; i++)
            {
                if (allFiles[i].Length > allFiles[longestNameIndex].Length)
                {
                    longestNameIndex = i;
                }
            }

            return allFiles[longestNameIndex];
        }
        
        public Double ProsjecanTecajHrkUsd()
        {
            StringBuilder url = new StringBuilder();
            url.Append("https://api.exchangeratesapi.io/history?")
               .Append("start_at=")
               .Append(DateTime.Today.AddDays(-7).ToString("yyyy-MM-dd"))
               .Append("&end_at=")
               .Append(DateTime.Today.ToString("yyyy-MM-dd"))
               .Append("&base=USD")
               .Append("&symbols=HRK");

            var client = new WebClient();
            var response = client.DownloadString(url.ToString());
            JObject parsedResponse = JObject.Parse(response);

            double sum = 0;
            int counter = 0;
            foreach (Double e in parsedResponse.SelectTokens("rates.*.HRK"))
            {
                sum += e;
                counter++;
            }

            return sum/counter;
        }

        public String[] Permutations(String input)
        {
            HashSet<String> tmpPermutations = new HashSet<string>();
            R(input.ToCharArray(), 0, input.Length - 1, tmpPermutations);

            String[] permutations = new String[tmpPermutations.Count];
            tmpPermutations.CopyTo(permutations);

            return permutations;
        }

        private void R(char[] input, int l, int r, HashSet<String> permutations)
        {
            if (l == r)
            {
                permutations.Add(new String(input));
            }
            else
            {
                for (int i = l; i <= r; i++)
                {
                    InnerSwitch(input, l, i);
                    R(input, l + 1, r, permutations);
                    InnerSwitch(input, i, l);
                }
            }
        }

        private void InnerSwitch(char[] input, int left, int right)
        {
            char tmp = input[left];
            input[left] = input[right];
            input[right] = tmp;
        }
    }
}